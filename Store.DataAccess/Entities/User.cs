﻿using Microsoft.AspNetCore.Identity;
using Store.DataAccess.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Store.DataAccess.Entities
{
    public class User : IdentityUser<long>
    {
        public User() : base()
        {
        }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        public Existence IsDeleted { get; set; }
        public DateTime CreationData { get; set; }

    }
}
