﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Store.DataAccess.Entities.Enums
{
    public enum Existence
    {
        None = 0,
        Deleted = 1,
        UnDeleted = 2
    }
}
