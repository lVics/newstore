﻿using Microsoft.AspNetCore.Identity;
using Store.DataAccess.Entities.Base;
using Store.DataAccess.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Store.DataAccess.Entities
{
    public class Role : IdentityRole<long>
    {

        public Existence IsDeleted { get; set; }
        public DateTime CreationData { get; set; }


    }
}
