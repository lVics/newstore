﻿using Store.DataAccess.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Store.DataAccess.Entities.Base
{
    public abstract class BaseEntity
    {
        [Key]

        public long Id { get; set; }
        public Existence IsDeleted { get; set; }
        public DateTime CreationData { get; set; }

        public BaseEntity()
        {
            IsDeleted = Existence.UnDeleted;
            CreationData = DateTime.Now;
        }


    }
}
