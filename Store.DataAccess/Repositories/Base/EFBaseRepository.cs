﻿
using Store.DataAccess.Initializations;
using System;
using System.Collections.Generic;


namespace Store.DataAccess.Repositories.Base
{
    public abstract class EFBaseRepository<T> : IRepository<T> where T : class
    {
        protected StoreContext db;

        public EFBaseRepository(StoreContext db)
        {
            this.db = db;
        }
        public void Save()
        {
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
            GC.SuppressFinalize(this);

        }
        public abstract IEnumerable<T> GetAll();
        public abstract T Get(long id);
        public abstract IEnumerable<T> Find(Func<T, bool> predicate);
        public abstract void Create(T item);
        public abstract void Update(T item);
        public abstract void Delete(long id);

    }
}
