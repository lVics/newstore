﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Store.DataAccess.Repositories.Base
{
    public interface IRepository<T> : IDisposable where T : class
    {
        IEnumerable<T> GetAll();
        T Get(long id);
        IEnumerable<T> Find(Func<T, Boolean> predicate);
        void Create(T item);
        void Update(T item);
        void Delete(long id);
        void Save();

    }
}
