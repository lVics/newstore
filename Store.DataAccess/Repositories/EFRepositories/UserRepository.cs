﻿using System;
using System.Collections.Generic;
using Store.DataAccess.Entities;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Store.DataAccess.Repositories.Base;
using Store.DataAccess.Initializations;
using Store.DataAccess.Entities.Enums;

namespace Store.DataAccess.Repositories.EFRepositories
{
    public class UserRepository : EFBaseRepository<User>
    {

        public UserRepository(StoreContext db) : base(db)
        {

        }

        public override void Create(User item)
        {
            db.Users.Add(item);
        }


        public override void Delete(long id)
        {

            if (db.Users.Find(id) != null)
            {
                db.Users.Find(id).IsDeleted = Existence.Deleted;
            }
        }

        public override IEnumerable<User> Find(Func<User, bool> predicate)
        {
            return db.Users.Where(predicate).ToList();
        }

        public override User Get(long id)
        {
            return db.Users.Find(id);
        }


        public override IEnumerable<User> GetAll()
        {
            return db.Users;
        }

        public override void Update(User item)
        {
            //db.Users.Update(item);

            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
