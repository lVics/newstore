﻿using Microsoft.EntityFrameworkCore;
using Store.DataAccess.Entities;
using Store.DataAccess.Entities.Enums;
using Store.DataAccess.Initializations;
using Store.DataAccess.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Store.DataAccess.Repositories.EFRepositories
{
    public class RoleRepository : EFBaseRepository<Role>
    {

        public RoleRepository(StoreContext db) : base(db)
        {

        }

        public override void Create(Role item)
        {
            db.Roles.Add(item);
        }

        public override void Delete(long id)
        {

            if (db.Roles.Find(id) != null)
            {
                db.Roles.Find(id).IsDeleted = Existence.Deleted;
            }
        }

        public override IEnumerable<Role> Find(Func<Role, bool> predicate)
        {
            return db.Roles.Where(predicate).ToList();
        }

        public override Role Get(long id)
        {
            return db.Roles.Find(id);
        }

        public override IEnumerable<Role> GetAll()
        {
            return db.Roles;
        }

        public override void Update(Role item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
