﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Store.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.DataAccess.Initializations
{
    public class StoreContext : IdentityDbContext<User, Role, long>
    {


        public StoreContext(DbContextOptions<StoreContext> options) : base(options) //
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<AuthorsInPrintingEdition>().HasKey(p => new { p.PrintingEditionId, p.AuthorId, p.Id });
        }

    }
}
