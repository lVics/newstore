﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Store.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Store.DataAccess.Initializations
{
    public static class IdentityDataInitializer
    {
        
        public static  void SeedRoleAsync(RoleManager<Role> roleManager)
        {
            
            if (!roleManager.RoleExistsAsync("USER").Result)
            {
                Role role = new Role {  Name = "USER", NormalizedName = "USER", IsDeleted = Entities.Enums.Existence.UnDeleted, CreationData = DateTime.Now };
                
                var result = roleManager.CreateAsync(role).Result;
                if (!result.Succeeded)
                {
                    throw new Exception("SeedRole USER unsucceed");
                }


            }
            if (!roleManager.RoleExistsAsync("ADMIN").Result)
            {
                Role role = new Role {  Name = "ADMIN", NormalizedName = "ADMIN", IsDeleted = Entities.Enums.Existence.UnDeleted, CreationData = DateTime.Now };
                var result = roleManager.CreateAsync(role).Result;
                if (!result.Succeeded)
                {
                    throw new Exception("SeedRole ADMIN unsucceed");
                }

            }
        }
    }
}
