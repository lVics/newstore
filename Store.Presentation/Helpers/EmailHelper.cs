﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Store.Presentation.Helpers
{
    public class EmailHelper
    {

        private readonly string _email;
        private readonly string _password;

        public EmailHelper(string email, string password)
        {
            _email = email;
            _password = password;
        }
        public void SendEmail(string email, string mess)
        {
            var client = new SmtpClient("Smtp.gmail.com");
            var from = new MailAddress(_email);
            var to = new MailAddress(email);
            var message = new MailMessage(from, to);
            message.Body = mess;
            client.Credentials = new NetworkCredential(_email, _password);
            client.EnableSsl = true;
            client.Send(message); // сделать ассинохронным
        }

    }
}
