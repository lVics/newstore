﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Store.BusinessLogic.Models.Users;
using Store.BusinessLogic.Services.Interfaces;
using Store.Presentation.Helpers;
using static System.Net.WebRequestMethods;

namespace Store.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService userService)
        {
            _accountService = userService;
        }

        [HttpPost, Route("SignUp")]
        public async Task<IActionResult> SignUp(UserModel userModel)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    await _accountService.UserRegisrtation(userModel);
                }
                catch (Exception e)
                {

                    return Ok(e.Message);
                }

                var user = _accountService.GetUser(userModel.Email, userModel.Password);


                string url = this.Url.Action("ConfirmEmail", "Account", new { id = user.Id }, protocol: HttpContext.Request.Scheme);
                var mail = new EmailHelper("thewalkingundeads@gmail.com", "1qaz2wsx3edc\\ "); // написать логин и пароль почты
                mail.SendEmail(userModel.Email, $"Подтвердите регистрацию, перейдя по ссылке: <a href='{url}'>link</a>");

                return Ok("Для завершения регистрации проверьте электронную почту и перейдите по ссылке, указанной в письме");
            }
            return Ok("Некорректно введены данные");

        }


        [HttpPost, Route("ConfirmEmail")]
        public async Task<IActionResult> ConfirmEmail(long id)
        {

            var user = _accountService.GetUser(id);

            _accountService.UserConfirm(id);
            return RedirectToAction("Get", "Values");
        }
        [HttpPost, Route("Login")]
        public async Task<IActionResult> Login(LoginUserModel loginUserModel)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    var user = _accountService.GetUser(loginUserModel.Email, loginUserModel.Password);
                    var userClaims = new[]
                    {
                          new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
                    };
                    var accessToken = JwtHelper.GenerateAccessToken(userClaims);
                    var refreshToken = JwtHelper.GenerateRefreshToken();
                    _accountService.EstablishToken(user.Id, accessToken, refreshToken);
                    return new ObjectResult(
                        new
                        {
                            accesstoken = accessToken,
                            refreshtoken = refreshToken
                        });
                }
                catch (Exception e)
                {

                    return Ok(e.Message);
                }


            }
            return Ok("Некорректно введены данные");
        }


        [HttpPost, Route("ChangeProfile")]
        public async Task<IActionResult> ChangeProfile(ChangeUserProfileModel changeUserProfileModel)
        {
            //           "UserLogin": "lVicsss",
            //"FirstName":"Nikitos",
            //"LastName":"Niet",
            //"Email":"qwerty@gmail.com",
            //"PhoneNumber":"+38888888",
            try
            {
                var principal = JwtHelper.GetPrincipalFromExpiredToken(changeUserProfileModel.Token);
                var claims = principal.Claims.ToList();
                var id = claims.Find(x => x.Type == ClaimTypes.NameIdentifier);
                var user = _accountService.GetUser(long.Parse(id.Value));
                if (user.Email == null)
                {
                    return Ok("Неправильный Access Token");
                }
                _accountService.ChangeUserParam(user.Id, changeUserProfileModel);
                return Ok("Выполнено успешно");
            }
            catch (Exception e)
            {

                return Ok(e.Message);
            }
        }

        [HttpPost, Route("ResetPassword")]
        public async Task<IActionResult> ResetPassword(ResetUserPasswordModel resetUserPasswordModel)
        {

            try
            {
                var principal = JwtHelper.GetPrincipalFromExpiredToken(resetUserPasswordModel.Token);
                var claims = principal.Claims.ToList();
                var id = claims.Find(x => x.Type == ClaimTypes.NameIdentifier);
                var user = _accountService.GetUser(long.Parse(id.Value));
                if (user.Email == null)
                {
                    return Ok("Неправильный Access Token");
                }
                _accountService.changePassword(user.Id, resetUserPasswordModel);
                return Ok("Выполнено успешно");

            }
            catch (Exception e)
            {

                return Ok(e.Message);
            }




        }
        [HttpPost, Route("ChangeToken")]
        public async Task<IActionResult> ChangeToken(ChangeUserTokenModel changeUserTokenModel)
        {
            try
            {

                var principal = JwtHelper.GetPrincipalFromExpiredToken(changeUserTokenModel.OldAccessToken);
                var claims = principal.Claims.ToList();
                var id = claims.Find(x => x.Type == ClaimTypes.NameIdentifier);
                var user = _accountService.GetUser(long.Parse(id.Value));
                if (user.Email == null)
                {
                    return Ok("Неправильный Access Token");
                }
                var accessToken = JwtHelper.GenerateAccessToken(principal.Claims);
                var refreshToken = JwtHelper.GenerateRefreshToken();
                _accountService.ChangeToken(user.Id, changeUserTokenModel.OldRefreshToken, refreshToken, accessToken);
                return new ObjectResult(new
                {
                    accesstoken = accessToken,
                    refreshtoken = refreshToken
                });
            }
            catch (Exception e)
            {

                return Ok(e.Message);
            }

        }

        [HttpPost, Route("DeleteToken")]
        public async Task<IActionResult> DeleteToken(string token)
        {
            try
            {

                var principal = JwtHelper.GetPrincipalFromExpiredToken(token);
                var claims = principal.Claims.ToList();
                var id = claims.Find(x => x.Type == ClaimTypes.NameIdentifier);
                var user = _accountService.GetUser(long.Parse(id.Value));
                if (user.Email == null)
                {
                    return Ok("Неправильный Access Token");
                }
                _accountService.DeleteToken(long.Parse(id.Value));
                return Ok("Выполнено успешно");
            }
            catch (Exception e)
            {

                return Ok(e.Message);
            }

        }


    }
}