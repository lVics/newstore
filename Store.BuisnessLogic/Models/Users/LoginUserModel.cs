﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Store.BusinessLogic.Models.Users
{
    public class LoginUserModel
    {
        
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
