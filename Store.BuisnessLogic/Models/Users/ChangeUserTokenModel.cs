﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Store.BusinessLogic.Models.Users
{
    public class ChangeUserTokenModel
    {
        [Required]
        public string OldAccessToken { get; set; }
        [Required]
        public string OldRefreshToken { get; set; }
    }
}
