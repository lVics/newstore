﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Store.BusinessLogic.Models.Users
{
    public class ResetUserPasswordModel
    {
        [Required]
        public string Password { get; set; }
        [Required]
        public string Token { get; set; }
    }
}
