﻿
using Store.BusinessLogic.Models.Users;
using System.Threading.Tasks;

namespace Store.BusinessLogic.Services.Interfaces
{
    public interface IAccountService
    {

        UserModel GetUser(string email, string password);
        UserModel GetUser(long id);
        UserModel GetUser(string email);
        Task UserRegisrtation(UserModel userModel);

        void ChangeUserParam(long id, ChangeUserProfileModel changeUserProfileModel);
        void RecoveryPassword(long id, string newPassword);

        void changePassword(long id, ResetUserPasswordModel resetUserPasswordModel);
        void UserConfirm(long id);
        void Save();
        void Dispose();
        void EstablishToken(long id, string accessToken, string refreshToken);
        void ChangeToken(long id, string oldRefreshToken, string newRefreshToken, string newAccessToken);

        void DeleteToken(long id);

    }
}
