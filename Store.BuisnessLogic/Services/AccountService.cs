﻿using Microsoft.AspNetCore.Identity;
using Store.BusinessLogic.Models.Users;
using Store.BusinessLogic.Services.Interfaces;
using Store.DataAccess.Entities;
using Store.DataAccess.Entities.Enums;
using Store.DataAccess.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Store.BusinessLogic.Services
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IRepository<User> _userRepository;
        private readonly RoleManager<Role> _roleManager;
        private readonly IRepository<Role> _roleRepository;

        public AccountService(UserManager<User> userManager, SignInManager<User> signInManager, IRepository<User> userRepository, RoleManager<Role> roleManager, IRepository<Role> roleRepository)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _userRepository = userRepository;
            _roleManager = roleManager;
            _roleRepository = roleRepository;
        }

        public void Dispose()
        {
            _userRepository.Dispose();
        }

        public UserModel GetUser(string email, string password)
        {
            var userlist = _userRepository.Find(x => x.Email == email);
            User user = new User();
            foreach (var item in userlist)
            {
                user = item;
            }
            if (user.Email == null)
            {
                throw new Exception("Неверно введен email ");
            }

            if (_userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, password) != PasswordVerificationResult.Success)
            {
                throw new Exception("Неверно введен пароль ");
            }
            UserModel userModel = new UserModel
            {
                Id = user.Id,
                Email = user.Email,
                Password = user.PasswordHash,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                UserLogin = user.UserName

            };

            return userModel;
        }
        public UserModel GetUser(string email)
        {
            var userlist = _userRepository.Find(x => x.Email == email);
            User user = new User();
            foreach (var item in userlist)
            {
                user = item;
            }
            if (user.Email == null)
            {
                throw new Exception("Неверно введен email ");
            }
            UserModel userModel = new UserModel
            {
                Id = user.Id,
                Email = user.Email,
                Password = user.PasswordHash,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                UserLogin = user.UserName

            };

            return userModel;
        }
        public UserModel GetUser(long id)
        {
            var user = _userRepository.Get(id);


            return new UserModel
            {

                Id = user.Id,
                Email = user.Email,
                Password = user.PasswordHash,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                UserLogin = user.UserName
            };
        }

        public async void RecoveryPassword(long id, string newPassword)
        {
            var user = _userRepository.Get(id);
            await _userManager.ChangePasswordAsync(user, await _userManager.GeneratePasswordResetTokenAsync(user), newPassword);

        }

        public void Save()
        {
            _userRepository.Save();
        }



        public async Task UserRegisrtation(UserModel userModel)
        {
            List<User> userlist = (List<User>)_userRepository.Find(x => x.Email == userModel.Email);
            User userCheck = userlist.Find(x => x.Email == userModel.Email);
            if (userCheck != null)
            {
                throw new Exception("пользователь с таким email уже существует");
            }
            userlist = (List<User>)_userRepository.Find(x => x.PhoneNumber == userModel.PhoneNumber);
            userCheck = userlist.Find(x => x.PhoneNumber == userModel.PhoneNumber);
            if (userCheck != null)
            {
                throw new Exception("пользователь с таким номером телефона уже существует");
            }
            userlist = (List<User>)_userRepository.Find(x => x.UserName == userModel.UserLogin);
            userCheck = userlist.Find(x => x.UserName == userModel.UserLogin);
            if (userCheck != null)
            {
                throw new Exception("пользователь с таким логином уже существует");
            }

            var user = new User
            {
                FirstName = userModel.FirstName,
                LastName = userModel.LastName,
                Email = userModel.Email,
                PhoneNumber = userModel.PhoneNumber,
                UserName = userModel.UserLogin,
                CreationData = DateTime.Now,
                IsDeleted = Existence.None

            };

            var role = _roleManager.FindByNameAsync("USER").Result;
            var resultRegistrate = _userManager.CreateAsync(user, userModel.Password).Result;
            var createUserToken = _userManager.GenerateTwoFactorTokenAsync(user, _userManager.Options.Tokens.AuthenticatorTokenProvider).Result;
            if (resultRegistrate.Succeeded)
            {
                var result = _userManager.AddToRoleAsync(user, role.Name.ToString().ToUpper()).Result; 
                await _signInManager.SignInAsync(user, false);
            }
            else
            {
                StringBuilder mes = new StringBuilder();
                foreach (var errors in resultRegistrate.Errors)
                {
                    mes.Append(errors + "\n\n");
                    throw new Exception(mes.ToString());
                }

            }


        }

        public void UserConfirm(long id)
        {
            var user = _userRepository.Get(id);
            user.IsDeleted = Existence.UnDeleted;
            _userRepository.Save();
        }

        public void EstablishToken(long id, string accessToken, string refreshToken)
        {
            var user = _userRepository.Get(id);
            var result = _userManager.SetAuthenticationTokenAsync(user, "Store", "AccessToken", accessToken).Result;
            if (!result.Succeeded)
            {
                throw new Exception("Ошибка при добавлении AccessToken");
            }
            result = _userManager.SetAuthenticationTokenAsync(user, "Store", "RefreshToken", refreshToken).Result;
            if (!result.Succeeded)
            {
                throw new Exception("Ошибка при добавлении RefreshToken");
            }

        }

        public void ChangeToken(long id, string oldRefreshToken, string newRefreshToken, string newAccessToken)
        {
            var user = _userRepository.Get(id);
            var refreshToken = _userManager.GetAuthenticationTokenAsync(user, "Store", "RefreshToken");
            if (refreshToken.Result == oldRefreshToken)
            {

                var result = _userManager.RemoveAuthenticationTokenAsync(user, "Store", "AccessToken").Result;
                if (!result.Succeeded)
                {
                    throw new Exception("Ошибка при удалении AccessToken");
                }
                result = _userManager.RemoveAuthenticationTokenAsync(user, "Store", "RefreshToken").Result;
                if (!result.Succeeded)
                {
                    throw new Exception("Ошибка при удалении RefreshToken");
                }

                result = _userManager.SetAuthenticationTokenAsync(user, "Store", "AccessToken", newAccessToken).Result;
                if (!result.Succeeded)
                {
                    throw new Exception("Ошибка при добавлении AccessToken");
                }
                result = _userManager.SetAuthenticationTokenAsync(user, "Store", "RefreshToken", newRefreshToken).Result;
                if (!result.Succeeded)
                {
                    throw new Exception("Ошибка при добавлении RefreshToken");
                }


            }
            else
            {
                throw new Exception("Неправильный RefreshToken");
            }

        }

        public void DeleteToken(long id)
        {
            var user = _userRepository.Get(id);
            var result = _userManager.RemoveAuthenticationTokenAsync(user, "Store", "AccessToken").Result;
            if (!result.Succeeded)
            {
                throw new Exception("Ошибка при удалении AccessToken");
            }
            result = _userManager.RemoveAuthenticationTokenAsync(user, "Store", "RefreshToken").Result;
            if (!result.Succeeded)
            {
                throw new Exception("Ошибка при удалении RefreshToken");
            }
        }

        public void ChangeUserParam(long id, ChangeUserProfileModel changeUserProfileModel)
        {
            var user = _userRepository.Get(id);
            if (user.Email == null)
            {
                throw new Exception("Неверый id пользователя");
            }

            List<User> userlist = (List<User>)_userRepository.Find(x => x.Email == changeUserProfileModel.Email);
            User userCheck = userlist.Find(x => x.Email == changeUserProfileModel.Email);
            if (userCheck != null)
            {
                throw new Exception("пользователь с таким email уже существует");
            }
            userlist = (List<User>)_userRepository.Find(x => x.PhoneNumber == changeUserProfileModel.PhoneNumber);
            userCheck = userlist.Find(x => x.PhoneNumber == changeUserProfileModel.PhoneNumber);
            if (userCheck != null)
            {
                throw new Exception("пользователь с таким номером телефона уже существует");
            }
            userlist = (List<User>)_userRepository.Find(x => x.UserName == changeUserProfileModel.UserLogin);
            userCheck = userlist.Find(x => x.UserName == changeUserProfileModel.UserLogin);
            if (userCheck != null)
            {
                throw new Exception("пользователь с таким логином уже существует");
            }
            user.Email = changeUserProfileModel.Email;
            user.FirstName = changeUserProfileModel.FirstName;
            user.LastName = changeUserProfileModel.LastName;
            user.UserName = changeUserProfileModel.UserLogin;
            user.PhoneNumber = changeUserProfileModel.PhoneNumber;
            var emailtoken = _userManager.GenerateChangeEmailTokenAsync(user, changeUserProfileModel.Email).Result;
            var result = _userManager.ChangeEmailAsync(user, changeUserProfileModel.Email, emailtoken).Result;
            if (!result.Succeeded)
            {
                throw new Exception("Ошибка при изменении Email через identity");
            }
            var phoneToken = _userManager.GenerateChangePhoneNumberTokenAsync(user, changeUserProfileModel.PhoneNumber).Result;
            result = _userManager.ChangePhoneNumberAsync(user, changeUserProfileModel.PhoneNumber, phoneToken).Result;
            if (!result.Succeeded)
            {
                throw new Exception("Ошибка при изменении PhoneNumber через identity");
            }
            _userRepository.Update(user);

        }

        public void changePassword(long id, ResetUserPasswordModel resetUserPasswordModel)
        {
            var user = _userRepository.Get(id);
            if (user == null)
            {
                throw new Exception("Неверый id пользователя");
            }
            user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, resetUserPasswordModel.Password);
            _userRepository.Update(user);
        }


    }
}
